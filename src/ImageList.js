import "./App.css";

function ImageList(props) {
  const { name } = props.image;

  return <img src={name} alt="Simran Pahwa" key={props.image.id}></img>;
}

export default ImageList;
