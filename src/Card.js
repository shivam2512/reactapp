import "./App.css";
import ImageList from "./ImageList";

const Card = (props) => {
  const renderimageCardlist = props.images.map((img, index) => {
    return (
      <div className="col-md-6 col-lg-4"  key={index}>
        <div className="item animated wow fadeIn">
          <ImageList image={img} key={index}></ImageList>
          <div className="overlay title-overlay">
            <div className="text">Simran Pahwa</div>
          </div>
        </div>
      </div>
    );
  });
  return <>{renderimageCardlist}</>;
};

export default Card;
