
import './App.css';
import Footer from "../src/Footer"
import Card from "./Card";

function App() {
  console.log('public url: ', process.env.PUBLIC_URL)

  const images_data = [
    {
      id: "1",
      name: process.env.PUBLIC_URL + "/images/10.png",
    },
    {
      id: "2",
      name:  process.env.PUBLIC_URL + "/images/11.png",
    },
    {
      id: "3",
      name: process.env.PUBLIC_URL + "/images/12.png",
    },
    {
      id: "4",
      name: process.env.PUBLIC_URL + "/images/13.png",
    },
    {
      id: "5",
      name: process.env.PUBLIC_URL + "/images/14.png",
    },
    {
      id: "6",
      name: process.env.PUBLIC_URL + "/images/15.png",
    }
  ];



  return (
    <div className="App">
      <header className="App-header" >
      {/* <h2 className='animate-charcter'>Wishing you <br/> a <br/> Happy New year</h2> */}
      <div className="lasvegas ">Hi ,
  Wish you a  <br/><span>Happy New Year</span><br/>
   <span className="delay">2022    &nbsp;</span><b className='emoji_rotate' style={{color:'#30231cf7'}}>&#128525;</b>
</div>


<section className="blog">
        <div className="container">
          <div className="row">
            <Card images={images_data}></Card>
          </div>
        </div>
      </section>

      </header>
      <Footer/>
    </div>
  );
}

export default App;
