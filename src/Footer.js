
import './App.css';

function Footer() {
 
  return (
    <footer className="site-footer">
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-sm-6 col-xs-12">
            <p className="copyright-text text-white">Copyright &copy; 2022 All Rights Reserved by 
         <span  className='text-white'>&nbsp;  Yatin Gulati </span><br/>
         Mail: Yatingulati594@gmail.com & Contact :  9953104995
            </p>
          </div>
{/* 
          <div className="col-md-4 col-sm-6 col-xs-12">
            <ul className="social-icons">
              <li><b><i className="fa fa-facebook text-white"></i></b></li>
              { <li><a className="twitter" target="_blank" rel="noopener noreferrer" href="www.google.com"><i className="fa fa-twitter text-white"></i></a></li> }
              <li><a className="linkedin" target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/public-profile/settings" ><i className="fa fa-linkedin text-white"></i></a></li>   
            </ul>
          </div> */}
        </div>
      </div>
</footer>
  );
}

export default Footer;
