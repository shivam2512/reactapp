
import './App.css';

function Footer() {
 
  return (
    <footer className="site-footer">
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-sm-6 col-xs-12">
            <p className="copyright-text">Copyright &copy; 2021 All Rights Reserved by 
         <a href="www.google.com">&nbsp;  Shiva Kumar Gone</a>.
            </p>
          </div>

          <div className="col-md-4 col-sm-6 col-xs-12">
            <ul className="social-icons">
              <li><a className="facebook" href="https://facebook.com/beingshivam2512" target="_blank" rel="noopener noreferrer"><i className="fa fa-facebook"></i></a></li>
              <li><a className="twitter" target="_blank" rel="noopener noreferrer" href="www.google.com"><i className="fa fa-twitter"></i></a></li>
              <li><a className="linkedin" target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/shiva-kumar-gone/21b21b115"><i className="fa fa-linkedin"></i></a></li>   
            </ul>
          </div>
        </div>
      </div>
</footer>
  );
}

export default Footer;
